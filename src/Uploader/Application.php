<?php namespace Uploader;

use Noodlehaus\Config;
use Silex\Application as SilexApplication;
use Silex\Application\TwigTrait;

class Application extends SilexApplication {
    use TwigTrait;

    /** @return Config */
    public function getConfig() {
        return $this['config'];
    }
}
