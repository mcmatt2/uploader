<!DOCTYPE html>
<html>
<head>
    <meta charset="utf8">
    <title>Uploader</title>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" type="text/css" href="theme.css">
</head>
<body>
<div class="container">
    <form action="." class="upload" enctype="multipart/form-data" method="post">
        <input class="textbox" name="file[]" type="file">

        <div>
            <input class="button" type="submit" value="Upload">
        </div>
    </form>
    {% if link %}
        <p>Done! Your files can be found at:</p>
        <ul>
            <li><a href="{{ link }}">{{ link }}</a></li>
        </ul>
    {% endif %}
    {% if errors %}
        <p>Sorry, the following errors occurred:</p>
        <ul>
            {% for error in errors %}
                <li>{{ error }}</li>
            {% endfor %}
        </ul>
    {% endif %}
</div>
</body>
</html>
