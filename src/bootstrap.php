<?php
$loader = require '../vendor/autoload.php';
$loader->addPsr4('Uploader\\', '../src/Uploader/');

use Noodlehaus\Config;
use Upload\File;
use Upload\FileInfo;
use Upload\Storage\FileSystem;
use Uploader\Application;

$app = new Application;

$app['config'] = $app->share(function (Application $app) {
    return Config::load('../config.json');
});
$app['debug'] = $app->getConfig()->get('debug', FALSE);

$app['twig'] = $app->share(function (Application $app) {
    $config = $app->getConfig();

    $loader = new Twig_Loader_Filesystem('../src/views');
    $twig = new Twig_Environment($loader, [
        'cache' => $config->get('path.cache'),
        'debug' => $app['debug'],
    ]);

    return $twig;
});

$app->get('/', function (Application $app) {
    return $app->render('home.tpl');
});

$app->post('/', function (Application $app) {
    $config = $app->getConfig();

    $new_filename = uniqid();
    $storage = new FileSystem($config->get('path.upload'));
    $file = new File('file', $storage);
    $file->beforeUpload(function (FileInfo $fileInfo) use ($new_filename) {
        $fileInfo->setName($new_filename);
    });

    $file->addValidations([
        new \Upload\Validation\Mimetype($config->get('validation.allowed_mime_types'))
    ]);

    try {
        $file->upload();

        /** @var \Upload\FileInfoInterface $uploadedFile */
        $uploadedFile = $file[0];
        $url = $config->get('path.public') . '/' . $uploadedFile->getNameWithExtension();

        return $app->render('home.tpl', [ 'link' => $url ]);
    } catch ( Exception $e ) {
        return $app->render('home.tpl', [ 'errors' => $file->getErrors() ]);
    }
});

return $app;
